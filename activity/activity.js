// 3. Insert a single room

db.rooms.insertOne(
{
	name: "Single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available : 10,
	isAvailable: false
}
);

// 4. Insert multiple rooms

db.rooms.insertMany(
[
	{
		name: "Double",
		accomodates: 3,
		price: 2000,
		description: "A room fit for a small family going on vacation",
		rooms_available : 5,
		isAvailable: false
	},
	{
		name: "Queen",
		accomodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		rooms_available : 15,
		isAvailable: false
	}
	
]
);

// 5. Use the find method

db.rooms.find({ name: "Double" })

// 6. Use the updateOne

db.rooms.updateOne(
{
    name: "Queen"
},
{
    $set : {
        rooms_available: 0
    }
}
);

// 7. use the deleteMany

db.rooms.deleteMany(
{
    rooms_available: 0
}
);